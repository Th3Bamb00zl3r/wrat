﻿using System;
using WRAT.Modules;

namespace WRAT
{
    class Program
    {
        static void Main(string[] args)
        {
            UIControl.PrintHeader();
            UIControl.NavigationMenu();
            Console.ReadLine();
        }
    }
}
