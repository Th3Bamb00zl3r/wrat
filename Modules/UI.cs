using System;
using System.Diagnostics;

namespace WRAT.Modules
{
    class UIControl
    {
        public static void Write(string Text, string Key, ConsoleColor KeyColor, bool NewLine)
        {
            Console.ForegroundColor = KeyColor;
            Console.Write($" [{Key}] ");
            Console.ForegroundColor = WRAT.Environment.DefaultConsoleForegroundColor;
            Console.Write(Text);
            if(NewLine)
            Console.WriteLine();
        }

        public static void WriteError(string ErrorText, bool NewLine)
        {
            Console.WriteLine();
            Console.Write(" ");
            Console.BackgroundColor = ConsoleColor.Red;
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(ErrorText);
            Console.BackgroundColor = WRAT.Environment.DefaultConsoleBackgroundColor;
            Console.ForegroundColor = WRAT.Environment.DefaultConsoleForegroundColor;
            if(NewLine)
                Console.WriteLine();
        }

        public static void PrintHeader()
        {
            Console.WriteLine(" ===================================================");
            Console.WriteLine(" = W.R.A.T - Windows Remote Administration Toolkit =");
            Console.WriteLine($" = Release.Version.Revision.Build: {GetVersion()} =");
            Console.WriteLine(" = Powerd by SysNuk3r (light version, no PEN)      =");
            Console.WriteLine(" ===================================================");
        }

        public static string GetVersion()
        {
            string buffer = string.Empty;
            buffer += WRAT.Modules.Text.FillLeft(WRAT.Environment.release.ToString(), 3, '0');
            buffer += ".";
            buffer += WRAT.Modules.Text.FillLeft(WRAT.Environment.version.ToString(), 3, '0');
            buffer += ".";
            buffer += WRAT.Modules.Text.FillLeft(WRAT.Environment.revision.ToString(), 3, '0');
            buffer += ".";
            buffer += WRAT.Modules.Text.FillLeft(WRAT.Environment.build.ToString(), 3, '0');
            return buffer;
        }

        public static void PrintHelp()
        {
            Console.WriteLine();
            Write("help    = display help", "+", ConsoleColor.Green, true);
            Write("version = print version", "+", ConsoleColor.Green, true);
            Write("sysinfo = print system information", "+", ConsoleColor.Green, true);
            Write("connect = connect to web-api-service", "+", ConsoleColor.Green, true);
            Write("exit    = exit application", "+", ConsoleColor.Green, true);
        }

        public static void NavigationMenu()
        {
            MenuFlag:
            Console.WriteLine();
            Write("Command = ", "~", ConsoleColor.Cyan, false);
            string cmd = Console.ReadLine();
            switch(cmd.ToLower())
            {
                case "help":
                    PrintHelp();
                goto MenuFlag;

                case "version":
                    PrintVersion();
                goto MenuFlag;
                case "exit":
                    throw new Exception("Exiting Application...");

                case "sysinfo":
                    GetSysInfo();
                goto MenuFlag;

                default:
                    WriteError("ERROR - No Valid Input Detected!", true);
                goto MenuFlag;
            }
        }

        public static void PrintVersion()
        {
            Console.WriteLine();
            Write($"Application Version: {GetVersion()}", "+", ConsoleColor.Green, true);
        }

        public static void GetSysInfo()
        {
            Console.WriteLine();
            if(WRAT.Environment.IsWindows())
            {
                Write($"    OS Platform: {System.Environment.OSVersion.Platform}", "+", ConsoleColor.Green, true);
                Write($"     OS Version: {System.Environment.OSVersion.VersionString}", "+", ConsoleColor.Green, true);
                Write($"       Username: {System.Environment.UserDomainName}\\{System.Environment.UserName}", "+", ConsoleColor.Green, true);
                Write($"Processor Cores: {System.Environment.ProcessorCount}", "+", ConsoleColor.Green, true);
                Write($" Is 64-Bit OS? : {System.Environment.Is64BitOperatingSystem}", "+", ConsoleColor.Green, true);
                Write($"   Machine Name: {System.Environment.MachineName}", "+", ConsoleColor.Green, true);
            }
        }
    }
}