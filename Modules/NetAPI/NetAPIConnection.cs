using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Net.WebSockets;

namespace WRAT.Modules.NetAPI
{
    class NetAPIConnection : IDisposable
    {
        private Socket SocketConnection;
        public NetAPIConnection(string APIAddress, string FirstRequestAddress) 
        {
            bool validAPIAddress = false;
            bool validFirstRequestAddress = false;
            if(CommonNetworking.ValidateUniversalURI(APIAddress) && CommonNetworking.TestURl(APIAddress)) { validAPIAddress = true; }
            if(CommonNetworking.ValidateUniversalURI(FirstRequestAddress) && CommonNetworking.TestURl(FirstRequestAddress)) { validFirstRequestAddress = true; }
            if(validAPIAddress)
            {
                WRAT.Modules.UIControl.Write("NetAPIConnection: APIAddress validated!", "+", ConsoleColor.Green, true);
                if(validFirstRequestAddress)
                {
                    WRAT.Modules.UIControl.Write("NetAPIConnection: FirstRequestAddress validated!", "+", ConsoleColor.Green, true);
                    OpenConnection(APIAddress, FirstRequestAddress);
                }
            }
        }

        private bool OpenConnection(string APIAddress, string FirstRequestAddress)
        {
            WRAT.Modules.UIControl.Write("Opening Connection...", "+", ConsoleColor.Yellow, true);
            return true;
        }

        public void Dispose()
        {

        }
    }
}
