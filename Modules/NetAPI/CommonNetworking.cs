using System;
using System.Net;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace WRAT.Modules.NetAPI
{
    class CommonNetworking
    {
        public static async Task<string> HttpGET(string URI)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URI);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            using(HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using(Stream stream = response.GetResponseStream())
            using(StreamReader reader = new StreamReader(stream))
            {
                return await reader.ReadToEndAsync();
            }
        }

        public static async Task<string> HttpPOST(string URI, string data, string contentType, string method = "POST")
        {
            byte[] dataBytes = Encoding.UTF8.GetBytes(data);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URI);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            request.ContentLength = dataBytes.Length;
            request.ContentType = contentType;
            request.Method = method;

            using (Stream requestBody = request.GetRequestStream())
            {
                requestBody.Write(dataBytes, 0, dataBytes.Length);
            }

            using(HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using(Stream stream = response.GetResponseStream())
            using(StreamReader reader = new StreamReader(stream))
            {
                return await reader.ReadToEndAsync();
            }
        }

        public static bool TestURl(string URI)
        {
            bool TestStatus = false;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URI);
            request.Timeout = 1000;
            request.Method = "HEAD";
            try
            {
                using(HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if(response.StatusCode == HttpStatusCode.OK)
                    {
                        TestStatus = true;
                        response.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                WRAT.Modules.UIControl.Write(ex.Message, "!", ConsoleColor.Red, true);
                return false;
            }
            return TestStatus;
        }

        public static bool ValidateUniversalURI(string URI)
        {
            Uri outUri;
            if(Uri.TryCreate(URI, UriKind.Absolute, out outUri))
            {
                return true;
            }
            return false;
        }
    }
}