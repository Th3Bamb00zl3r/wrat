using System;
using System.Text;

namespace WRAT.Modules
{
    class Text
    {
        public static string FillLeft(string Text, int PreferredLength, char FillChar)
        {
            int text_length = Text.Length;
            int fillRequestLength = PreferredLength - text_length;
            string fillString = string.Empty;
            for(int i=0;i<fillRequestLength;i++)
            {
                fillString += FillChar;
            }
            Text = fillString + Text;
            return Text;
        }
    }
}