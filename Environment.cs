using System;
using System.Runtime.InteropServices;

namespace WRAT
{
    class Environment
    {
        public static readonly ConsoleColor DefaultConsoleForegroundColor = ConsoleColor.White;
        public static readonly ConsoleColor DefaultConsoleBackgroundColor = ConsoleColor.Black;
        public static readonly int release = 0;
        public static readonly int version = 19;
        public static readonly int revision = 3;
        public static readonly int build = 242;

        public static bool IsWindows() => RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
        public static bool IsMacOs() => RuntimeInformation.IsOSPlatform(OSPlatform.OSX);
        public static bool IsLinux() => RuntimeInformation.IsOSPlatform(OSPlatform.Linux);

    }
}